<?php
/**
 * Plugin Name: Ride-a-Boda Custom Post Types
 * Description: Handles the creation of custom post types for the ride-a-boda website
 * Author: Joanita K Tushabe
 * Version: 1.0
 **/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

	/**
	 * Post Type: Boda Bodas.
	 */ 

add_action ( 'init', 'boda_boda_post_type' );

function boda_boda_post_type() {
	
	register_post_type( "Boda Bodas", array (
		'labels' => array (
			"name" => __( "Boda Bodas", "twentytwentyone" ),
			"singular_name" => __( "Boda Boda", "twentytwentyone"),
			"add_new" => __( "Add New", "twentytwentyone"),
			"add_new_item" => __( "Add New Boda Boda Location", "twentytwentyone"),
			"edit_item" => __( "Edit Boda Boda Location", "twentytwentyone"),
			"new_item" => __( "New Boda Boda Location", "twentytwentyone"),
			"view_item" => __( "View Boda Boda Location", "twentytwentyone"),
			"view_items" => __( " View Boda Boda Locations", "twentytwentyone"),
			"search_items" => __( "Search Boda Boda Locations", "twentytwentyone"),
			"not_found" => __( "No Boda Boda Location found", "twentytwentyone"),
			"not_found_in_trash" => __( "No Boda Boda Location found in trash", "twentytwentyone"),
			"all_items" => __( "All Boda Boda Locations", "twentytwentyone"),
			"archives" => __( "Boda Boda Locations Archives", "twentytwentyone"),
			"insert_into_item" => __( "Insert into Boda Boda Location", "twentytwentyone"),
			"uploaded_to_this_item" => __( "Uploaded to this Boda Boda Location", "twentytwentyone"),
			"filter_item_list" => __( "Filter Boda Boda List", "twentytwentyone"),
			"items_list_navigation" => __( "Boda Boda Locations list navigation", "twentytwentyone"),
			"items_list" => __( "Boda Boda Location List", "twentytwentyone"),
			"item_published" => __( "Published Boda Boda Locations", "twentytwentyone"),
			"item_published_privately" => __( "Privately Published Boda Boda Locations", "twentytwentyone"),
			"item_reverted_to_draft" => __( "Boda Boda Location reverted to draft", "twentytwentyone"),
			"item_scheduled" => __( "Boda Boda Location Scheduled", "twentytwentyone"),
			"item_updated" => __( "Published Boda Boda Location Updated", "twentytwentyone")
		),

		"description" => "",
		"has_archive" => true,
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_rest" => true,
		"show_in_nav_menus" => true,
		"supports" => array('title', 'editor', 'thumbnail', 'revisons', 'custom-fields'),
		"can_export" => true
	));
}

//Boda Boda Location Field 
	function add_post_meta_boxes() {
		add_meta_box(
			"post_metadata_boda_boda_post",
			"Boda Boda Location",
			"post_meta_box",
			"Boda Bodas",
			"side",
			"low"
		);
	}
add_action("admin_init", "add_post_meta_boxes");

//Save field value
function save_post_meta_boxes() {
	global $post;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
		return;
	}
	update_post_meta($post ->ID, "boda_boda_location", sanitize_text_field($_POST["boda_boda_location"] )); 
}

add_action('save_post', 'save_post_meta_boxes');

/*	function post_meta_box() {
		global $post;
		$custom = get_post_custom( $post-> ID );
		$data = $custom["_boda_boda_location"][0];
		echo "<input type= \"text\" name= \"_boda_boda_location\" value=\ "".$data."\" placeholder =\ "Boda_Boda _Location\">";
	}
	*/