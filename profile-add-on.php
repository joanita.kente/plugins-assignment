<?php
/**
 * Plugin Name: Profile Add On
 * Description: Adds aditional user data to the profile screen
 * Author: Joanita K Tushabe
 * Version: 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_61dc20724af1e',
		'title' => 'Additional User Data',
		'fields' => array(
			array(
				'key' => 'field_61dc208fd9e0e',
				'label' => 'Age',
				'name' => 'user_age',
				'type' => 'number',
				'instructions' => 'Enter your age',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array(
				'key' => 'field_61dc22b18bd65',
				'label' => 'Sex',
				'name' => 'sex',
				'type' => 'select',
				'instructions' => 'Enter your sex',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'Male' => 'Male',
					'Female' => 'Female',
				),
				'default_value' => false,
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'return_format' => 'value',
				'ajax' => 0,
				'placeholder' => '',
			),
			array(
				'key' => 'field_61dc23248bd66',
				'label' => 'Preferred Programming Language',
				'name' => 'preferred_programming_language',
				'type' => 'text',
				'instructions' => 'Enter your programming language of choice',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => 'Javascript',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_61dc23ace5555',
				'label' => 'Preferred Content Management System',
				'name' => 'preferred_content_management_system',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => 'wordpress',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'user_form',
					'operator' => '==',
					'value' => 'all',
				),
				array(
					'param' => 'user_role',
					'operator' => '==',
					'value' => 'all',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'seamless',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
		'show_in_rest' => 0,
	));
	
	endif;		

